import 'package:flutter/material.dart';
import 'views/dashboard_route.dart';
import 'routes.dart';
void main(){
  runApp(PixleApp());
}

class PixleApp extends StatelessWidget{
  //define color for later use
  static const Color kreatorkuColor = Color.fromARGB(255, 218, 56, 104);
  static const Color KreatorkuWhite = Colors.white;
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pixle',
      theme: ThemeData(
        primaryColor: kreatorkuColor,
        accentColor: KreatorkuWhite,
      ),
      //home: DashboardRoute(),
      routes: routes,
    );
  }
}