import 'package:flutter/material.dart';
import 'views/login_route.dart';
import 'views/dashboard_route.dart';

final routes = {
  '/login': (BuildContext context) => new LoginRoute(),
  '/home' : (BuildContext context) => new DashboardRoute(),
  '/': (BuildContext context) => new LoginRoute(),
};