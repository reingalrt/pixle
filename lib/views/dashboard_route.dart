/*
  Maintainer: -Oktaviano
              -Enter your name here
*/

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:pixle/main.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

PixleApp pixleApp = new PixleApp();

class DashboardRoute extends StatefulWidget{
  const DashboardRoute();

  @override
  _DashboardRouteState createState() => _DashboardRouteState();
}

class _DashboardRouteState extends State<DashboardRoute>{
  //define variable for firebase requirements
  StreamSubscription<QuerySnapshot> subscription;
  List<DocumentSnapshot> imageList;
  final CollectionReference collectionReference = Firestore.instance.collection("images");

  //if state initializede, do subscripting to data snapshot
  @override
  void initState() {
    super.initState();
    subscription = collectionReference.orderBy("likes", descending: true).snapshots().listen((datasnapshot){
      setState(() {
        imageList = datasnapshot.documents;
      });
    });
  }

  //else dispose subscription to cancel the action
  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //create tab bar
    final tabBar = DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(text: 'home'),
              Tab(text: 'more'),
              Tab(text: 'profile'),
            ],
          ),
          title: Text('Dashboard'),
        ),
          body: TabBarView(children: [
            new Scaffold(
              body: imageList != null ?
              new StaggeredGridView.countBuilder(
                  padding: const EdgeInsets.all(8.0),
                  crossAxisCount: 4,
                  itemCount: imageList.length,
                  itemBuilder: (context, i){
                    String imgPath = imageList[i].data['url'];
                    return new Material(
                      elevation: 8.0,
                      borderRadius: new BorderRadius.all(new Radius.circular(8.0)),
                      child: new InkWell(
                        child: new Hero(
                            tag: imgPath,
                            child: new FadeInImage(
                              image: new NetworkImage(imgPath),
                              fit: BoxFit.cover,
                              placeholder: new AssetImage("assets/ic_kreator_transparent.png"),
                            ),
                        ),
                      ),
                    );
                  },
                  staggeredTileBuilder: (i) => new StaggeredTile.count(2, i.isEven?2:3),
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
              ) : new Center(
                child: CircularProgressIndicator(),
              )
            ),
            Text('more'),
            Text('Profile'),],
          ),
      ),
    );

    return Scaffold(
      body: tabBar,
    );
  }
}