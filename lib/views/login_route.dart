import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:pixle/auth.dart';
import 'package:pixle/data/database_helper.dart';
import 'package:pixle/models/user.dart';
import 'package:pixle/views/login/login_view_presenter.dart';
import 'package:pixle/main.dart';

class LoginRoute extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new LoginRouteState();
  }
}

class LoginRouteState extends State<LoginRoute> implements LoginScreenContract, AuthStateListener{
  BuildContext _context;

  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  String _username, _password;

  LoginScreenPresenter _presenter;

  LoginScreenState(){
    _presenter = new LoginScreenPresenter(this);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  void _submit(){
    final form = formKey.currentState;

    if(form.validate()){
      setState(() {
        _isLoading = true;
      });
      form.save();
      _presenter.doLogin(_username, _password);
    }
  }

  void _showSnackBar(String text){
    scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(text)));
  }

  @override
  void onAuthStateChanged(AuthState state) {
    // TODO: implement onAuthStateChanged
    if(state == AuthState.LOGGED_IN)
      Navigator.of(_context).pushReplacementNamed("/home");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _context = context;
    var loginBtn = new RaisedButton(
      onPressed: _submit,
      child: new Text("Masuk"),
      color: PixleApp.kreatorkuColor,
    );
    var loginForm = new Column(
      children: <Widget>[
        new Text(
          "Pixle",
          textScaleFactor: 2.0,
        ),
        new Form(
            key: formKey,
            child: new Column(
              children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    onSaved: (val) => _username = val,
                    validator: (val){
                      return val.length < 5 ? "Username harus lebih dari 5 karakter" : null;
                      },
                    decoration: new InputDecoration(labelText: "Username"),
                  ),
                ),
                new Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    onSaved: (val) => _password = val,
                    decoration: new InputDecoration(labelText: "Password"),
                  ),
                ),
              ],
            ),
        ),
        _isLoading ? new CircularProgressIndicator() : loginBtn
      ],
      crossAxisAlignment: CrossAxisAlignment.center,
    );

    return new Scaffold(
      appBar: null,
      key: scaffoldKey,
      body: new Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
              image: new AssetImage("assets/ic_kreator_transparent.png"),
              fit: BoxFit.cover
          ),
        ),
        child: new Center(
          child: new ClipRect(
            child: new BackdropFilter(filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: new Container(
              child: loginForm,
              height: 300.0,
              width: 300.0,
              decoration: new BoxDecoration(
                color: Colors.grey.shade200.withOpacity(0.5)
              ),
            ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void OnLoginError(String errorTxt) {
    // TODO: implement OnLoginError
    _showSnackBar(errorTxt);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void OnLoginSuccess(User user) async {
    // TODO: implement OnLoginSuccess
    _showSnackBar(user.toString());
    setState(() {
      _isLoading = false;
    });
    var db = new DatabaseHelper();
    await db.saveUser(user);
    var authStateProvider = new AuthStateProvider();
    authStateProvider.notify(AuthState.LOGGED_IN);
  }
}