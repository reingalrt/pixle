import 'package:pixle/data/rest_ds.dart';
import 'package:pixle/models/user.dart';

abstract class LoginScreenContract{
  void OnLoginSuccess(User user);
  void OnLoginError(String errorTxt);
}

class LoginScreenPresenter{
  LoginScreenContract _view;
  RestDatasource api = new RestDatasource();
  LoginScreenPresenter(this._view);

  doLogin(String username, String password){
    api.login(username, password).then((User user){
      _view.OnLoginSuccess(user);
    }).catchError((Exception error) => _view.OnLoginError(error.toString()));
  }
}